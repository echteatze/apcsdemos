import scala.io.Source

object FindLongLines1 {

  def processFile(fileName : String, width : Int) = {
    val source = Source.fromFile(fileName)
    for(line <- source.getLines()) processLine(fileName, width, line)
  }

  def processLine(fileName : String, width : Int, line : String) = {
    if (line.length > width) println(fileName + ": " + line.trim)
  }

  def main(args : Array[String]) = {
    val width = args(0).toInt
    for(arg <- args.drop(1)) processFile(arg,width)
  }
}

object FindLongLines2 {
  // place function in other function

  def processFile(fileName : String, width : Int) = {
    def processLine(fileName : String, width : Int, line : String) = {
      if (line.length > width) println(fileName + ": " + line.trim)
    }

    val source = Source.fromFile(fileName)
    for(line <- source.getLines()) processLine(fileName, width, line)
  }



  def main(args : Array[String]) = {
    val width = args(0).toInt
    for(arg <- args.drop(1)) processFile(arg,width)
  }
}

object FindLongLines3 {
  // can use variables from context

  def processFile(fileName : String, width : Int) = {
    def processLine(line : String) = {
      if (line.length > width) println(fileName + ": " + line.trim)
    }

    val source = Source.fromFile(fileName)
    for(line <- source.getLines()) processLine(line)
  }



  def main(args : Array[String]) = {
    val width = args(0).toInt
    for(arg <- args.drop(1)) processFile(arg,width)
  }
}


object FindLongLines4 {
  // can also be declared later

  def processFile(fileName : String, width : Int) = {
    val source = Source.fromFile(fileName)
    for(line <- source.getLines()) processLine(line)

    def processLine(line : String) = {
      if (line.length > width) println(fileName + ": " + line.trim)
    }
  }



  def main(args : Array[String]) = {
    val width = args(0).toInt
    for(arg <- args.drop(1)) processFile(arg,width)
  }
}