import scala.collection.mutable
/* This is the reverse polish calculator
   in imperative style.
 */


object ReversePolish {

  def isNumber(e: String): Boolean = {
    for(c <- e.toCharArray){
      if(!c.isDigit) return false
    }
    return true
  }

  def calculate(expression: String): Int = {
    val s = new mutable.Stack[Int]
    for(e <- expression.split(" ")){
      if(e == "+" || e == "-" || e == "*" || e == "/"){
        val rhs = s.pop()
        val lhs = s.pop()
        val res = e match {
          case "+" => lhs + rhs
          case "-" => lhs - rhs
          case "*" => lhs * rhs
          case "/" => lhs / rhs
        }
        s.push(res)
      } else if(isNumber(e)) s.push(e.toInt)
      else throw new Error("Invalid expression " + e)
    }
    s.pop()
  }

  def calculateAndPrint(expression : String) : Unit = {
    println("Calculating " + expression )
    println("Result : " + calculate(expression))
  }

  def main(args : Array[String]) = {
    calculateAndPrint("1 1 +")
    calculateAndPrint("1 2 + 3 4 5 - - * 6 7 8 - * /")
  }
}
