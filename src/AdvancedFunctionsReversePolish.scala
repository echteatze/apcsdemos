import scala.collection.immutable.Stack
/* This is the reverse polish calculator
   in imperative style.

   Modified for readable code lecture

   Modified to use immutable data

   Modified to use objects, inheritance
 */


abstract class Expression {
  def value : Int
  def describe = toString + " = " + value.toString
}

case class NumberExpression(n : Int) extends Expression {
  def value: Int = n
  override def toString = n.toString
}

case class OperatorExpression(lhs : Expression,
                              op : String,
                              rhs : Expression ) extends Expression {
  def value: Int =
    OOReversePolish.
      getOperatorByName(op)(lhs.value, rhs.value)

  override def toString =
    "(" + lhs.toString + op + rhs.toString + ")"
}

object OOReversePolish {

  def isNumber(e: String): Boolean = {
    for(c <- e.toCharArray){
      if(!c.isDigit) return false
    }
    return true
  }

  def isOperatorName(name : String) =
    name == "+" || name == "-" || name == "*" || name == "/"

  def getOperatorByName(name : String) : (Int, Int) => Int =
    name match {
      case "+" => _ + _
      case "-" => _ - _
      case "*" => _ * _
      case "/" => _ / _
    }
  // or operatorByName(name)

  val operatorsByName : Map[String, (Int,Int) => Int]  =
    Map("+" -> (_ + _),
        "-" -> (_ - _),
        "*" -> (_ * _),
        "/" -> (_ / _))





  def parse(expression: String): Expression = {

    // local methods below mutate this variable
    var stack = new Stack[Expression]

    def handleNumber( e : String) = {
      stack = stack.push(NumberExpression(e.toInt))
    }

    def handleElement( e : String)  = {
      if(isOperatorName(e))stack = handleOperator(e)
      else if(isNumber(e))  stack  = handleNumber( e)
      else throw new Error("Invalid expression " + e)
    }

    def handleOperator( e : String) =
    {
      val rhs = stack.top
      stack = stack.pop
      val lhs = stack.top
      stack = stack.pop
      val res = OperatorExpression(lhs, e, rhs)
      stack.push(res)
    }

    for(e <- expression.split(" ")){
      handleElement( e)
    }
    stack.top
  }


  def main(args : Array[String]) = {
    println(parse("1").describe)
    println(parse("1 1 +").describe)
    println(parse("1 2 + 3 4 5 - - * 6 7 8 - * /").describe)
  }
}
