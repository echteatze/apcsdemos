class Rational(var numerator : Int, var denominator : Int) {
  //println("This is initialization code  " + numerator + "/" + denominator)

  def this(numerator : Int) = this(numerator,1)
  override def toString = numerator.toString + "/" +denominator.toString

  def getDenom = denominator
  def add(rhs : Rational) : Rational =
    new Rational(this.numerator * rhs.denominator + rhs.numerator * this.denominator,
      this.denominator * rhs.denominator)

  def +(rhs : Rational) = this.add(rhs)

  def <(rhs :Rational) =
    numerator * rhs.denominator < rhs.numerator * denominator

  def max(rhs : Rational) =
    if (this < rhs) rhs else this
}


object Test {
  def main(args : Array[String]) = {
    val one : Rational = new Rational(1)
    val a : Rational = new Rational(2,3)
    val b : Rational = new Rational(1,3)
    println(a.max(b))
  }
}