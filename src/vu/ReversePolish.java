package vu;

import vu.expressions.BinaryOperator;
import vu.expressions.Expression;
import vu.expressions.Number;

import java.util.Stack;


public class ReversePolish {

    Expression toExpression(String expression) {
        Stack<Expression> stack = new Stack<>();
        for(String elem : expression.split(" ")){
            if(isOperator(elem)){
                Expression x = stack.pop();
                Expression y = stack.pop();
                Expression res = new BinaryOperator(x,elem,y);
                stack.push(res);
            } else if(isNumber(elem)) {

                stack.push(new Number(Integer.parseInt(elem)));
            } else {
                throw new Error("I do not know how to handle "+ expression);
            }
        }
        return stack.peek();
    }

    private boolean isNumber(String s) {
        for(char c : s.toCharArray()) {
            if(!Character.isDigit(c)) return false;
        }
        return  true;
    }


    private boolean isOperator(String elem) {
        return elem.equals("+") || elem.equals("-")
                || elem.equals("*") || elem.equals("/");
    }



    public static void main(String[] argv){
        ReversePolish polish = new ReversePolish();
        polish.handleExpression("2 3 +");
        polish.handleExpression("3 4 5 * +");
        polish.handleExpression("1 2 + 4 * 5 + 3 -");
        polish.handleExpression("15 7 1 1 + - / 3 * 2 1 1 + + -");
    }

    private void handleExpression(String s) {
        System.out.println(toExpression(s).describe());
    }
}
