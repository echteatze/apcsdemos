package vu.expressions;

public class BinaryOperator extends Expression{
    final Expression lhs, rhs;
    final String op;


    public BinaryOperator(Expression lhs, String op, Expression rhs) {
        this.lhs = lhs;
        this.op = op ;
        this.rhs = rhs;
    }

    public String toString() {
        return "(" + lhs.toString() + " " + op + " " + rhs.toString() + ")";
    }

    @Override
    public int value() {
        int x = lhs.value();
        int y = rhs.value();
        switch(op) {
            case "+" : return x + y;
            case "-" : return x - y;
            case "*" : return x * y;
            case "/" : return x / y;
            default : throw new Error("Unkown operator" + op);
        }
    }
}
