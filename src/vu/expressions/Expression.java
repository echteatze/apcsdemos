package vu.expressions;

public abstract class Expression {

    public abstract int value();

    public String describe() {
        return toString() + " = " + value();
    }
}
