package vu.expressions;

public class Number extends Expression {
    final int number;

    public Number(int number) {
        this.number = number;
    }

    @Override
    public int value() {
        return number;
    }

    public String toString() {
        return "" + number;
    }
}
