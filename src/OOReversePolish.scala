import scala.collection.immutable.Stack
/* This is the reverse polish calculator
   in imperative style.

   Modified for readable code lecture

   Modified to use immutable data

   Modified to use objects, inheritance
 */


abstract class Expression {
  def value : Int
  def describe = toString + " = " + value.toString
}

case class NumberExpression(n : Int) extends Expression {
  def value: Int = n
  override def toString = n.toString
}

case class OperatorExpression(lhs : Expression,
                              op : String,
                              rhs : Expression ) extends Expression {
  def value: Int =
    OOReversePolish.computeOperatorByName(op,lhs.value, rhs.value)

  override def toString =
    "(" + lhs.toString + op + rhs.toString + ")"
}

object OOReversePolish {

  def isNumber(e: String): Boolean = {
    for(c <- e.toCharArray){
      if(!c.isDigit) return false
    }
    return true
  }

  def isOperatorName(name : String) =
    name == "+" || name == "-" || name == "*" || name == "/"

  def computeOperatorByName(name : String, lhs : Int, rhs : Int) = {
    name match {
      case "+" => lhs + rhs
      case "-" => lhs - rhs
      case "*" => lhs * rhs
      case "/" => lhs / rhs
    }
  }

  def handleOperator(initialStack : Stack[Expression], e : String) =
  {
    var stack = initialStack
    val rhs = stack.top
    stack = stack.pop
    val lhs = stack.top
    stack = stack.pop
    val res = OperatorExpression(lhs, e, rhs)
    stack.push(res)
  }

  def handleNumber(stack : Stack[Expression], e : String) :Stack[Expression]= {
    stack.push(NumberExpression(e.toInt))
  }

  def handleElement(stack : Stack[Expression], e : String) : Stack[Expression] = {
    if(isOperatorName(e))handleOperator(stack,e)
    else if(isNumber(e)) handleNumber(stack, e)
    else throw new Error("Invalid expression " + e)
  }

  def parse(expression: String): Expression = {
    var stack = new Stack[Expression]
    for(e <- expression.split(" ")){
      stack = handleElement(stack, e)
    }
    stack.top
  }


  def main(args : Array[String]) = {
    println(parse("1").describe)
    println(parse("1 1 +").describe)
    println(parse("1 2 + 3 4 5 - - * 6 7 8 - * /").describe)
  }
}
