import scala.collection.mutable
/* This is the reverse polish calculator
   in imperative style.

   Modified for readable code lecture
 */


object ReversePolish {

  def isNumber(e: String): Boolean = {
    for(c <- e.toCharArray){
      if(!c.isDigit) return false
    }
    return true
  }

  def isOperatorName(name : String) =
    name == "+" || name == "-" || name == "*" || name == "/"

  def computeOperatorByName(name : String, lhs : Int, rhs : Int) = {
    name match {
      case "+" => lhs + rhs
      case "-" => lhs - rhs
      case "*" => lhs * rhs
      case "/" => lhs / rhs
    }
  }

  def handleOperator(stack : mutable.Stack[Int], e : String) =
    {
      val rhs = stack.pop()
      val lhs = stack.pop()
      val res = computeOperatorByName(e,lhs,rhs)
      stack.push(res)
    }

  def handleNumber(stack : mutable.Stack[Int], e : String) = {
    stack.push(e.toInt)
  }

  def calculate(expression: String): Int = {
    val stack = new mutable.Stack[Int]
    for(e <- expression.split(" ")){
      if(isOperatorName(e))handleOperator(stack,e)
      else if(isNumber(e)) handleNumber(stack, e)
      else throw new Error("Invalid expression " + e)
    }
    stack.pop()
  }

  def calculateAndPrint(expression : String) : Unit = {
    println("Calculating " + expression )
    println("Result : " + calculate(expression))
  }

  def main(args : Array[String]) = {
    calculateAndPrint("1 1 +")
    calculateAndPrint("1 2 + 3 4 5 - - * 6 7 8 - * /")
  }
}
