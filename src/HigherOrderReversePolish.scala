import scala.collection.immutable.Stack
/* This is the reverse polish calculator
   in imperative style.

   Modified for readable code lecture

   Modified to use immutable data

   Modified to use fold left
 */




object ImmutableReversePolish {

  def isNumber(e: String): Boolean = {
    for(c <- e.toCharArray){
      if(!c.isDigit) return false
    }
    return true
  }

  def isOperatorName(name : String) =
    name == "+" || name == "-" || name == "*" || name == "/"

  def computeOperatorByName(name : String, lhs : Int, rhs : Int) = {
    name match {
      case "+" => lhs + rhs
      case "-" => lhs - rhs
      case "*" => lhs * rhs
      case "/" => lhs / rhs
    }
  }

  def handleOperator(initialStack : Stack[Int], e : String) =
  {
    var stack = initialStack
    val rhs = stack.top
    stack = stack.pop
    val lhs = stack.top
    stack = stack.pop
    val res = computeOperatorByName(e,lhs,rhs)
    stack.push(res)
  }

  def handleNumber(stack : Stack[Int], e : String) :Stack[Int]= {
    stack.push(e.toInt)
  }

  def handleElement(stack : Stack[Int], e : String) : Stack[Int] = {
    if(isOperatorName(e))handleOperator(stack,e)
    else if(isNumber(e)) handleNumber(stack, e)
    else throw new Error("Invalid expression " + e)
  }

  def calculate(expression: String): Int = {
    expression.split(" ").
      foldLeft(Stack[Int]())(handleElement).
      top

  }

  def calculateAndPrint(expression : String) : Unit = {
    println("Calculating " + expression )
    println("Result : " + calculate(expression))
  }

  def main(args : Array[String]) = {
    calculateAndPrint("1 1 +")
    calculateAndPrint("1 2 + 3 4 5 - - * 6 7 8 - * /")
  }
}
