
import java.util.ArrayList;

public class MunchHausen {

    ArrayList<Integer> digits(int l) {
        ArrayList<Integer> res = new ArrayList<>();
        while(l!=0){
            res.add((int) l%10);
            l = l / 10;
        }
        return res;
    }


    boolean isMuchHausen(int l) {
        ArrayList<Integer> ds = digits(l);
        long sum = 0;
        for(int d : digits(l)){
            sum += (int)Math.pow(d,d);
        }
        return l == sum;
    }

    ArrayList<Integer> muchHausenNumbers() {
        ArrayList<Integer> res = new ArrayList<>();
        for(int i = 0 ; i < 100_000; i++){
            if(isMuchHausen(i)){
                res.add(i);
            }
        }
        return res;
    }

    void printMunchHausenNumbers() {
        for(int i : muchHausenNumbers()){
            if(isMuchHausen(i)){
                System.out.println("" + i);
            }
        }
    }

    public static void main(String[] argv) {
        new MunchHausen().printMunchHausenNumbers();
    }
}
