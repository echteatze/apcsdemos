object CountOccurrences {

  def foldLeft[A,B](l : List[A], z : B, f : (B,A) => B) : B = {
    var res = z
    for(e <- l) res = f(res,e)
    res
  }

  // example implementation of fold
  def fold[A](l: List[A], z : A, f: (A,A) => A) : A = {
    var res = z
    for(e <- l) res = f(res,e)
    res
  }

  def updateMap( m : Map[Char,Int], e : Char) : Map[Char,Int] =
  {
    val currentCount : Int = m.getOrElse(e,0)
    val newMapping : (Char,Int) = (e,currentCount + 1)
    m + newMapping
  }

  def countOccurancesImperative(l : List[Char]) : Map[Char,Int]  = {
    var m = Map[Char,Int]()
    for(e <- l) {
      m = updateMap(m,e)
    }
    return m
  }

  def countOccurancesFoldLeft(l : List[Char]) : Map[Char,Int] = {
    foldLeft(l,Map.empty[Char,Int], updateMap)
  }


  def countOccurances(l : List[Char]) : Map[Char,Int] = {
    l.map(e => Map(e -> 1)).
      fold(Map.empty)(mergeMaps)
  }

  // mergeMaps is an associative function with identity element:
  // empty map
  def mergeMaps(lhs : Map[Char,Int], rhs : Map[Char,Int]) = {
    def getTotal(e : Char) : Int =
      lhs.getOrElse(e,0) + rhs.getOrElse(e,0)
    (lhs.keySet ++ rhs.keySet)
      .map(e => (e,getTotal(e))).toMap
  }


  def main(argv : Array[String]) = {
    println(countOccurancesImperative("aabbabcacab".toList))
  }

}


