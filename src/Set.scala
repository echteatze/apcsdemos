case class Set[A](private val elements : Vector[A] ) {
  def +(rhs : Set[A]) : Set[A] = {
    var res : Vector[A] = elements
    for(e <- rhs.elements) {
      if(!elements.contains(e)) {
        res = e +: res
      }
    }
    Set(res)
  }

  def contains(e : Int) = elements.contains(e)
}

object Set {
  def empty = Set(Vector())
  def singleton[A] (e : A) : Set[A] = Set(Vector(e))

  def main(args : Array[String]) = {
    var set  : Set[Int] = singleton(5) + singleton(3) + singleton(6) + singleton(5)
    var setb : Set[Boolean] = singleton(true) + singleton(false) + singleton(true)

    println(setb)
  }
}